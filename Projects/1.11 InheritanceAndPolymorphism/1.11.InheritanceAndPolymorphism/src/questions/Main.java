package questions;

public class Main {

    public static void main(String[] args) {
        Engine engine = new Engine(235);
        Car car = new Car(engine);

        System.out.println(car.getEngine().getPower());
    }
}
