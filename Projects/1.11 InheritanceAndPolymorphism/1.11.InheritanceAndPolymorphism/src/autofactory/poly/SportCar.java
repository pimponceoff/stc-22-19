package autofactory.poly;

public class SportCar extends Car {

    private boolean isTurbo;

    public SportCar(String brand, String model, boolean isTurbo) {
        super(brand, model);
        this.isTurbo = isTurbo;
    }

    public void turboOn() {
        System.out.println("Мы включили ускорение");
    }
    @Override
    public void drive() {
        System.out.println("Вжууууууууух");
    }

    public boolean isTurbo() {
        return isTurbo;
    }

    public void setTurbo(boolean turbo) {
        isTurbo = turbo;
    }
}
