package oop;
//Класс - это чертеж, на основании которого будут собираться объекты
public class TestClass {
    //Поля класса
    public String text;
    public int number;
    public boolean isTrue;
    //Название конструктора всегда должно совпадать с названием класса
    //Alt + insert


    public TestClass(String text, int number, boolean isTrue) {
        this.text = text;
        this.number = number;
        this.isTrue = isTrue;
    }

    public TestClass(String text) {
        this.text = text;
    }

    public TestClass() {
    }

    public void sayHello() {
        System.out.println("Ребята, привет, мы на уроке программирования");
    }

    public void something() {
        if (number < 100) {
            System.out.println("<100");
        } else {
            System.out.println(">100");
        }
    }
}
