package newVersion.anonymClasses;

import java.util.Random;

public class Main {

    public static void main(String[] args) {
        int[] array = new int[10];
        Random random = new Random();
        for (int i = 0; i < array.length; i++) {
            //Создаются числа от 0 до 9, и прибавляется 1, что бы числа были от 1 до 10
            array[i] = random.nextInt(10) + 1;
        }

        //Создаем анонимный класс -> создаем его объект и кладем ссылку на него в объектную переменную интерфейса
        NumberUtils numberUtils = new NumberUtils() {
            @Override
            public void checkNumbers(int[] array) {
                System.out.println("Четные числа");
                for (int i = 0; i < array.length; i++) {
                    if (array[i] % 2 == 0) {
                        System.out.println(array[i] + " - четное число");
                    }
                }
            }
        };
        //Вызываем метод объекта анонимного класса
        numberUtils.checkNumbers(array);

        //Создаем новый анонимный класс -> создаем его объект и кладем ссылку на него в объектную переменную интерфейса
        numberUtils = new NumberUtils() {
            @Override
            public void checkNumbers(int[] array) {
                System.out.println("Нечетные числа");
                for (int i = 0; i < array.length; i++) {
                    if (array[i] % 2 != 0) {
                        System.out.println(array[i] + " - нечетное число");
                    }
                }
            }
        };
        //Вызываем метод объекта нового анонимного класса
        numberUtils.checkNumbers(array);
    }
}
