package oldVersion;

import java.util.Random;

public class Main {

    public static void main(String[] args) {
        int[] array = new int[10];
        Random random = new Random();
        for (int i = 0; i < array.length; i++) {
            //Создаются числа от 0 до 9, и прибавляется 1, что бы числа были от 1 до 10
            array[i] = random.nextInt(10) + 1;
        }
        //В переменную интерфейса кладем ссылку на объект класса, который реализует данный интерфейс
        UtilNumbers utilNumbers = new EvenNumbers();
        System.out.println("Только четные:");
        //Вызываем метод объекта
        utilNumbers.checkNumbers(array);
        //В переменную интерфейса кладем ссылку на новый объект класса, который так же реализует данный интерфейс
        utilNumbers = new OddNumbers();
        System.out.println("Только нечетные:");
        //Вызываем метод объекта
        utilNumbers.checkNumbers(array);
    }

    //Метод, определяющий четные числа и нечетные
    public static void checkNumbers(int[] array) {
        for (int i = 0; i < array.length; i++) {
            if (array[i] % 2 == 0) {
                System.out.println(array[i] + " - четное число");
            } else {
                System.out.println(array[i] + " - нечетное число");
            }
        }
    }
}
