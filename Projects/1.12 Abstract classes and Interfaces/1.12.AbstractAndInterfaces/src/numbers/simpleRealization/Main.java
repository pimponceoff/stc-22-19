package numbers.simpleRealization;

public class Main {

    public static void main(String[] args) {
        Number number = new Number();

        for (int i = 0; i <= 5; i++) {
            System.out.println(i + " - " + number.isFive(i));
        }
    }
}
