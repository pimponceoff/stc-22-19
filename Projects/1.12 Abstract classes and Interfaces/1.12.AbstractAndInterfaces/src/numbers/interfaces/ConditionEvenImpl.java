package numbers.interfaces;

public class ConditionEvenImpl implements Condition{
    @Override
    public boolean isOk(int number) {
        return number % 2 == 0;
    }
}
