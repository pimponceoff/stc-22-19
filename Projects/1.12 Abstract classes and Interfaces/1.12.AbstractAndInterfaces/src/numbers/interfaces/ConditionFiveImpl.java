package numbers.interfaces;

public class ConditionFiveImpl implements Condition {
    @Override
    public boolean isOk(int number) {
        return number % 5 == 0;
    }
}
