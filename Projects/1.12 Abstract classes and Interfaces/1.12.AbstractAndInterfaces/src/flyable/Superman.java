package flyable;

public class Superman implements Flyable {
    @Override
    public void fly() {
        System.out.println("Я - Супермен, я тоже умею летать");
    }
}
