package interfaces;

public class MyInterfaceImpl implements MyInterface {

    @Override
    public void hello() {
        System.out.println("Hello, we are in MyInterfaceImpl");
    }

    @Override
    public void goodbay() {
        System.out.println("Goodbay, we are in MyInterfaceImpl");
    }

    @Override
    public void preHello() {

    }

}
