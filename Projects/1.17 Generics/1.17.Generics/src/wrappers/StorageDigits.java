package wrappers;

public class StorageDigits<T> {

    private T[] storageDigits = (T[]) new Object[10];

    private int count;

    public StorageDigits() {
        this.count = 0;
    }

    public boolean addElement(T number) {
        if (count < storageDigits.length) {
            storageDigits[count] = number;
            count++;
            return true;
        }

        return false;
    }
}
