package generics.oldVersion.firstSolution;

public class MailBox {

    private Mail mail;

    public MailBox(Mail mail) {
        this.mail = mail;
    }

    public Mail getMail() {
        return mail;
    }

    public void setMail(Mail mail) {
        this.mail = mail;
    }
}
