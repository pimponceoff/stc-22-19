package generics.oldVersion.secondSolution;

import generics.oldVersion.firstSolution.Magazine;

public class MailBox {

    private Mail mail;

    private Box box;

    private Magazine magazine;

    public MailBox(Mail mail) {
        this.mail = mail;
    }

    public MailBox(Box box) {
        this.box = box;
    }

    public MailBox(Magazine magazine) {
        this.magazine = magazine;
    }

    public MailBox() {}

    public Mail getMail() {
        return mail;
    }

    public void setMail(Mail mail) {
        this.mail = mail;
    }

    public Box getBox() {
        return box;
    }

    public void setBox(Box box) {
        this.box = box;
    }

    public Magazine getMagazine() {
        return magazine;
    }

    public void setMagazine(Magazine magazine) {
        this.magazine = magazine;
    }
}
