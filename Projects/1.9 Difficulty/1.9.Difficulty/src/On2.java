import java.util.Arrays;

public class On2 {


    public static void main(String[] args) {
        //tableMultiplication(10);
        int[] array = {1, 9, 18, 2, 19, 0};
        bubbleSort(array);
    }

    /**
     * Метод, который распечатывает таблицу умножения от 1 до числа, которое передается в метод
     * @param number - число, до которого делать таблицу умножения
     *
     */
    public static void tableMultiplication(int number) {
        for (int i = 1; i <= number; i++) {
            for (int j = 1; j <= number; j++) {
                System.out.print(i * j + " ");
            }
            System.out.println();
        }
    }
    //Сортировка Пузырьком - самые большие числа (или меньшие, в зависимости от знака сравнения
    // на 31 строке) последовательно "всплывают" в начало массива
    //O(n^2)
    public static void bubbleSort(int[] array) {
        for (int i = 0; i < array.length; i++) {
            for (int j = i; j < array.length; j++) {
                if (array[i] < array[j]) {
                    int temp = array[j];
                    array[j] = array[i];
                    array[i] = temp;
                    System.out.println(Arrays.toString(array));
                }
            }
        }
    }

}
