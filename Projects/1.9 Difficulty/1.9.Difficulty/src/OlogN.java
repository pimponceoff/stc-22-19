public class OlogN {
    public static void main(String[] args) {
        //675

        int[] array = {0, 2, 4, 6, 78, 155, 987, 1459};

        System.out.println(recursiveBinarySearch(array, 987, 0, array.length - 1));
    }

    //O(log n) - логарифмическая зависимость
    public static int recursiveBinarySearch(int[] array, int elementToSearch, int beginIndex, int lastIndex) {
        // Если последний индекс больше или равен начальному
        if (lastIndex >= beginIndex) {
            //Высчитываем середину между двух индексов
            int mid = beginIndex + (lastIndex - beginIndex) / 2;

            // Если элемент по индексу, который мы высчитали и есть искомый элемент,
            // то мы его возвращаем
            if (array[mid] == elementToSearch) {
                return mid;
            }

            // Если элемент по индексу, который мы высчитали больше элемента, который мы ищем
            // то вызываем этот же метод, но отбрасываем большую (правую) половину массива
            // т.е. ищем число в левой части массива
            if (array[mid] > elementToSearch) {
                return recursiveBinarySearch(array, elementToSearch, beginIndex, mid - 1);
            }

            // Если элемент по индексу, который мы высчитали меньше элемента, который мы ищем
            // то вызываем этот же метод, но отбрасываем меньшую (левую) половину массива
            // т.е. ищем число в правой части массива
            if (array[mid] < elementToSearch) {
                return recursiveBinarySearch(array, elementToSearch, mid + 1, lastIndex);
            }

        }

        //Если число, которое мы ищем мы не нашли - то возвращаем -1
        return -1;
    }
}
