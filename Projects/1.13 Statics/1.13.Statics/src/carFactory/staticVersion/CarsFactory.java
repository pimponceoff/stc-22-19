package carFactory.staticVersion;

public class CarsFactory {

    private static int vin;
    public Car createCar(String brand, String model) {
        Car car = new Car(brand, model, vin);
        vin = vin + 1;
        return car;
    }
}
