package references;

import java.util.Arrays;

public class Swap {

    public static void main(String[] args) {
        int[] array = {11, 16, 0, 100, 145};
        int number = 10;

        System.out.println(Arrays.toString(array));
        swapNumbers(array, 0, 4);
        System.out.println(Arrays.toString(array));
        swapNumbers(array, 1, 3);
        System.out.println(Arrays.toString(array));
    }

    public static void swapNumbers(int[] array, int index1, int index2) {
        int temp = array[index1];
        array[index1] = array[index2];
        array[index2] = temp;
    }
}
