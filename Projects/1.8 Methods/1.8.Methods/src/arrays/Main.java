package arrays;

import java.util.Random;

public class Main {
    public static void main(String[] args) {
        int length = getRandomNumber(100);
        int[] array = new int[length];
        System.out.println(array.length);
        //int[] array = new int[getRandomNumber(100)];

    }

    public static int getRandomNumber(int highNumber) {
        Random random = new Random();

        int result = random.nextInt(highNumber);

        return result;
    }
}
